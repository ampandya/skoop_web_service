﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace SkoopOrders.Controllers
{
    [Produces("application/json")]
    [Route("api/Orders")]
    public class OrdersController : Controller
    {
        // GET: api/Orders
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "Skoop!", "Kiosk", "Orders" };
        }

        // GET: api/Orders/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Orders
        [HttpPost]
        public object Post([FromBody]string value)
        {
            
            Request.Body.Position = 0;
            string textReply = new System.IO.StreamReader(Request.Body).ReadToEnd();
            System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiRequestReceived.log", textReply);
            Process[] pname = Process.GetProcessesByName("Touch");
            if (pname.Length == 0)
            {
                string cApiKey = textReply.Substring(textReply.IndexOf("api_key") + 10, 20);
                string cGroupNo = textReply.Substring(textReply.IndexOf("groupNumber") + 14, 6);
                string cMerchant = textReply.Substring(textReply.IndexOf("merchantID") + 13, 10);

                //string OrdResponse = @"c:\Touch\data\OLOSkoopOrderReply.txt";
                string cReplytext = @"{""api_key"":""";
                cReplytext += cApiKey;
                cReplytext += @""", ""status"":""0"",""method"":""OLOSubmitOrderResponse"",""groupNumber"":""";
                cReplytext += cGroupNo;
                cReplytext += @""",""merchantID"":""";
                cReplytext += cMerchant;
                cReplytext += @""",""errorCode"":""999"",""message"":""Touch Program is not Running, Pleae contact the operator""}";

                System.IO.File.WriteAllText(@"c:\Touch\data\OLOSkoopOrderReply.txt", cReplytext);
                string texttoReply = System.IO.File.ReadAllText(@"c:\Touch\data\OLOSkoopOrderReply.txt");

                Orders ordfdel = new Orders();
                ordfdel.fDelete(@"c:\Touch\data\OLOSkoopOrderReply.txt");

                var obj2 = Newtonsoft.Json.JsonConvert.DeserializeObject(texttoReply);
                System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiResponse.log", texttoReply);
                return obj2;
            }
            
            //Request.Body.Position = 0;
            //string textReply = new System.IO.StreamReader(Request.Body).ReadToEnd();

            string data = JObject.Parse(textReply)["method"].ToString();
            string replyfromSubmitOrder = "";

            if (data == "OLOSubmitOrder")
            {
                Orders Or = new Orders();                
                replyfromSubmitOrder = Or.submit(textReply);
                System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiResponse.log", textReply);
            }

            if (data == "OLOCancelOrder")
            {
                Orders Or = new Orders();
                replyfromSubmitOrder = Or.cancelOrder(textReply);
                System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiResponse.log", textReply);
            }

            if (data == "OLOCloseOrder")
            {
                Orders Or = new Orders();
                replyfromSubmitOrder = Or.close(textReply);
            }

            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(replyfromSubmitOrder);
            System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiResponse.log", replyfromSubmitOrder);
            return obj;
        }
        
        // PUT: api/Orders/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
