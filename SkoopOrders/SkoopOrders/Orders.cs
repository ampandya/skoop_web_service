﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace SkoopOrders
{
    public class KioskOrder
    {
        public string api_key { get; set; }
        public string method { get; set; }
        public string groupNumber { get; set; }
        public string merchantID { get; set; }
        public string clerk { get; set; }
        public string printer { get; set; }
        public string orderNumber { get; set; }
        public string takeoutFlag { get; set; }
        public string tip { get; set; }
        public string freeTex { get; set; }
        public IList<ItemList> items { get; set; }
        
        public class ItemList
        {
            public string plu { get; set; }
            public string qty { get; set; }
            public string delete { get; set; }
            public string instructions { get; set; }
            public IList<OptionsList> options { get; set; }
        }
        
        public class OptionsList
        {
            public string plu { get; set; }
            public string qty { get; set; }
        }

    }

    public class KioskCloseOrder
    {
        public string api_key { get; set; }
        public string method { get; set; }
        public string groupNumber { get; set; }
        public string merchantID { get; set; }
        public string clerk { get; set; }        
        public string orderNumber { get; set; }        
        public string tip { get; set; }        
        public IList<MediaList> media { get; set; }

        public class MediaList
        {
            public string media { get; set; }
            public string amount { get; set; }            
        }        
    }
    public static class ExceptionExtensions
    {
        public static string GetFullMessage(this Exception ex)
        {
            return ex.InnerException == null
                 ? ex.Message
                 : ex.Message + " --> " + ex.InnerException.GetFullMessage();
        }
    }
    public class Orders
    {

        public string submit(string suborder)
        {
            //System.IO.File.WriteAllText(@"c:\Touch\data\OLOSkoopOrderReceived.txt", suborder);
            string ordernumber = "";
            string Price1 = "";
            decimal nPrice = 0;
            int newordnumber = 0;
            string serverpath = "";

            try
            {

                using (OleDbConnection concontrol = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\"))
                {
                    var sql = "select * from control";
                    OleDbCommand cmd = new OleDbCommand(sql, concontrol);
                    concontrol.Open();
                    DataSet ds = new DataSet();
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    if (dt.Rows.Count >= 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            serverpath = dr["SERVER"].ToString();
                            break;
                        }
                    }
                    concontrol.Close();
                }

                //using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))

                //using (OleDbConnection con = new OleDbConnection(@"Provider =VFPOLEDB.1;Data Source=" + serverpath.Trim() + ";Extended Properties=dBASE IV;User ID=Admin;"))
                using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath.Trim() + ";Extended Properties=dBASE IV;User ID=Admin;"))
                {
                    var sql = "select * from STUB_COUNTER";

                    OleDbCommand cmd = new OleDbCommand(sql, con);

                    con.Open();

                    DataSet ds = new DataSet();
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];

                    if (dt.Rows.Count >= 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ordernumber = dr["STUB"].ToString();
                            break;
                        }
                        newordnumber = Convert.ToInt16(ordernumber) + 1;

                        var sql1 = "update stub_counter set stub =" + newordnumber;
                        OleDbCommand cmd1 = new OleDbCommand(sql1, con);
                        DataSet ds1 = new DataSet();
                        OleDbDataAdapter da1 = new OleDbDataAdapter(cmd1);
                        da1.Fill(ds1);
                        con.Close();
                    }
                }



                KioskOrder KO = JsonConvert.DeserializeObject<KioskOrder>(suborder);

                if (KO.orderNumber == null) { KO.orderNumber = newordnumber.ToString(); }
                if (KO.tip == null) { KO.tip = ""; }
                if (KO.tip == null) { KO.tip = ""; }
                if (KO.freeTex == null) { KO.freeTex = ""; }
                if (KO.printer == null) { KO.printer = ""; }

                int clinenumber = 1;
                foreach (var barcodes in KO.items)
                {
                    string CINSERTVALUES = "'" + Convert.ToString(clinenumber) + "','" + KO.clerk.ToString() + "'" +
                        ",'" + KO.orderNumber.ToString() + "'" + ",'" + KO.tip.ToString() +
                        "','" + KO.printer.ToString() + "'" + ",'" + KO.takeoutFlag.ToString() +
                        "'" + ",'" + KO.freeTex.ToString() + "','" + Convert.ToString(barcodes.plu) +
                        "','" + Convert.ToString(barcodes.qty) + "','" + Convert.ToString(barcodes.delete) +
                        "','" + Convert.ToString(barcodes.instructions) + "','','','','','','','','',''";

                    using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                    {
                        var sql = "INSERT INTO OLOSKOOPORDER VALUES (" + CINSERTVALUES + ")";
                        OleDbCommand cmd = new OleDbCommand(sql, con);
                        con.Open();
                        DataSet ds = new DataSet();
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(ds);
                        con.Close();
                    }

                    foreach (var barcode in barcodes.options)
                    {
                        clinenumber = clinenumber + 1;
                        CINSERTVALUES = "'" + Convert.ToString(clinenumber) + "','" + KO.clerk.ToString() + "'" +
                                           ",'" + KO.orderNumber.ToString() + "'" + ",'" + KO.tip.ToString() +
                                           "','" + KO.printer.ToString() + "'" + ",'" + KO.takeoutFlag.ToString() +
                                           "'" + ",'" + KO.freeTex.ToString() + "','" + Convert.ToString(barcode.plu) +
                                           "','" + Convert.ToString(barcode.qty) + "','" + Convert.ToString(barcodes.delete) +
                                           "','" + Convert.ToString(barcodes.instructions) + "','','','','','','','','',''";

                        using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                        //using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                        {
                            var sql = "INSERT INTO OLOSKOOPORDER VALUES (" + CINSERTVALUES + ")";
                            OleDbCommand cmd = new OleDbCommand(sql, con);
                            con.Open();
                            DataSet ds = new DataSet();
                            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                            da.Fill(ds);
                            con.Close();
                        }
                    }
                    clinenumber = clinenumber + 1;
                }

                foreach (var barcodes in KO.items)
                {
                    string FindPLU = barcodes.plu;

                    //using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                    using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                    {
                        var sql = "select * from STOCK_PRICELEVELS.DBF where CPLU ='" + FindPLU + "'";
                        OleDbCommand cmd = new OleDbCommand(sql, con);
                        con.Open();
                        DataSet ds = new DataSet(); ;
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        if (dt.Rows.Count >= 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                Price1 = dr["nPrice"].ToString();
                                nPrice = nPrice + (Convert.ToDecimal(Price1) * Convert.ToDecimal(barcodes.qty));
                                break;
                            }
                            con.Close();
                        }
                    }

                    foreach (var barcode in barcodes.options)
                    {
                        string FindPLUOption = barcode.plu;
                        string FindMasterSku = "";
                        string FindMasterSkuPLevel = "";

                        //using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                        using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                        {
                            var sql = "select * from STOCK_ALT.DBF where AltSku ='" + FindPLUOption + "'";
                            OleDbCommand cmd = new OleDbCommand(sql, con);
                            con.Open();
                            DataSet ds = new DataSet(); ;
                            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                            da.Fill(ds);
                            DataTable dt = new DataTable();
                            dt = ds.Tables[0];
                            if (dt.Rows.Count >= 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    FindMasterSku = dr["MasterSku"].ToString();
                                    FindMasterSkuPLevel = dr["Prilev"].ToString();                                    
                                    break;
                                }
                            }
                        }

                        if(FindMasterSku == "")
                        {
                            using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                            {
                                var sql = "select * from STOCK_PRICELEVELS.DBF where CPLU ='" + FindPLUOption + "'";
                                OleDbCommand cmd = new OleDbCommand(sql, con);
                                con.Open();
                                DataSet ds = new DataSet(); ;
                                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                                da.Fill(ds);
                                DataTable dt = new DataTable();
                                dt = ds.Tables[0];
                                if (dt.Rows.Count >= 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        Price1 = dr["nPrice"].ToString();
                                        nPrice = nPrice + (Convert.ToDecimal(Price1) * Convert.ToDecimal(barcode.qty));
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                            {
                                var sql = "select * from STOCK_PRICELEVELS.DBF where CPLU ='" + FindMasterSku.Trim() + "' and nPriceLvL =" + FindMasterSkuPLevel + "";                                
                                OleDbCommand cmd = new OleDbCommand(sql, con);
                                con.Open();
                                DataSet ds = new DataSet(); ;
                                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                                da.Fill(ds);
                                DataTable dt = new DataTable();
                                dt = ds.Tables[0];
                                if (dt.Rows.Count >= 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        Price1 = dr["nPrice"].ToString();
                                        nPrice = nPrice + (Convert.ToDecimal(Price1) * Convert.ToDecimal(barcode.qty));
                                        break;
                                    }
                                }
                            }
                        }


                    }
                }

                decimal thisisfinalprice = nPrice;

                using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                //using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=" + serverpath))
                {
                    var sql = "update oloskooporder set ntotalamt='" + Convert.ToString(thisisfinalprice) + "' where clineno = '1' and nordno ='" + Convert.ToString(newordnumber) + "'";
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    con.Open();
                    DataSet ds = new DataSet(); ;
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                    con.Close();
                }

                string textReply = "";
                /*
                //string data = JObject.Parse(textReply)["method"].ToString();
                System.IO.File.WriteAllText(@"c:\Touch\data\OLOSkoopOrder.txt", suborder);
                string fullPath = @"c:\Touch\data\OLOSkoopOrderReply.txt";
                while (true)
                {
                    try
                    {
                        using (System.IO.StreamReader stream = new StreamReader(fullPath))
                        {
                            textReply = System.IO.File.ReadAllText(fullPath);
                            //string textReplyString = Newtonsoft.Json.JsonConvert.SerializeObject(textReply);                                           
                            break;
                        }
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                File.Delete(fullPath);
                */
                textReply = @"{""api_key"":""" + KO.api_key.ToString();
                textReply += @""", ""status"":""1"",""method"":""OLOSubmitOrderResponse"",""groupNumber"":""" + KO.groupNumber.ToString();
                textReply += @""",""merchantID"":""" + KO.merchantID.ToString();
                textReply += @""",""orderNumber"":""" + newordnumber.ToString();
                textReply += @""",""subtotal"":""" + Convert.ToString(Convert.ToInt32(thisisfinalprice * 100));
                textReply += @"""}";

                return textReply;
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"c:\Touch\logs\skoop\WebApiException.txt", ex.Message + "  --   " + ExceptionExtensions.GetFullMessage(ex));
                return "";
            }
            
            }

        public string close(string subcloseorder)
        {
            try
            {

                KioskCloseOrder KCO = JsonConvert.DeserializeObject<KioskCloseOrder>(subcloseorder);

                string textReply = "";
                /*
                System.IO.File.WriteAllText(@"c:\Touch\data\OLOSkoopCloseOrder.txt", subcloseorder);
                string fullPath = @"c:\Touch\data\OLOSkoopCloseOrderReply.txt";
                while (true)
                {
                    try
                    {
                        using (System.IO.StreamReader stream = new StreamReader(fullPath))
                        {
                            textReply = System.IO.File.ReadAllText(fullPath);
                            //textReply = Newtonsoft.Json.JsonConvert.SerializeObject(textReply);
                            break;
                        }
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                File.Delete(fullPath);
                */
                int cnt = 1;
                foreach (var clOrd in KCO.media)
                {
                    using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                    {
                        var sql = "update oloskooporder set nmedia" + cnt.ToString() + "='" + Convert.ToString(clOrd.media) + "', namt" + cnt.ToString() + "='" + Convert.ToString(clOrd.amount) + "' where clineno = '1' and nordno ='" + KCO.orderNumber.ToString() + "'";
                        OleDbCommand cmd = new OleDbCommand(sql, con);
                        con.Open();
                        DataSet ds = new DataSet(); ;
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(ds);
                        con.Close();
                    }
                    cnt = cnt + 1;
                }

                textReply = @"{""api_key"":""" + KCO.api_key.ToString();
                textReply += @""", ""status"":""1"",""method"":""OLOCloseOrderResponse"",""groupNumber"":""" + KCO.groupNumber.ToString();
                textReply += @""",""merchantID"":""" + KCO.merchantID.ToString();
                textReply += @""",""orderNumber"":""" + KCO.orderNumber.ToString();
                textReply += @"""}";

                return textReply;
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"c:\Touch\logs\WebApiException.txt", ex.Message + "  --   " + ExceptionExtensions.GetFullMessage(ex));
                return "";
            }
        }
        public string cancelOrder(string cancelorder)
        {
            try
            {
                KioskCloseOrder KCO = JsonConvert.DeserializeObject<KioskCloseOrder>(cancelorder);

                string textReply = "";
                /*System.IO.File.WriteAllText(@"c:\Touch\data\OLOSkoopCancelOrder.txt", cancelorder);
                string fullPath = @"c:\Touch\data\OLOSkoopCancelOrderReply.txt";
                while (true)
                {
                    try
                    {
                        using (System.IO.StreamReader stream = new StreamReader(fullPath))
                        {
                            textReply = System.IO.File.ReadAllText(fullPath);
                            //textReply = Newtonsoft.Json.JsonConvert.SerializeObject(textReply);
                            break;
                        }
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                File.Delete(fullPath);*/

                using (OleDbConnection con = new OleDbConnection(@"Provider=VFPOLEDB.1;Data Source=C:\Touch\Data\"))
                {
                    var sql = "delete from oloskooporder where nordno ='" + KCO.orderNumber.ToString() + "'";
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    con.Open();
                    DataSet ds = new DataSet(); ;
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                    con.Close();
                }

                textReply = @"{""api_key"":""" + KCO.api_key.ToString();
                textReply += @""", ""status"":""1"",""method"":""OLOCancelOrderResponse"",""groupNumber"":""" + KCO.groupNumber.ToString();
                textReply += @""",""merchantID"":""" + KCO.merchantID.ToString();
                textReply += @""",""orderNumber"":""" + KCO.orderNumber.ToString();
                textReply += @"""}";

                return textReply;
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"c:\Touch\logs\WebApiException.txt", ex.Message + "  --   " + ExceptionExtensions.GetFullMessage(ex));
                return "";
            }
        }
        public void fDelete(string fname)
        {
            try
            {
                File.Delete(fname);
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"c:\Touch\logs\WebApiException.txt", ex.Message + "  --   " + ExceptionExtensions.GetFullMessage(ex));
                
            }
        }

    }

}
